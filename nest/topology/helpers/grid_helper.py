# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2019-2020 NITK Surathkal

import sys
from nest.experiment import *
from nest.topology import *

##############################
# Topology: Grid helper with user-specified number of hosts.

#
#
#	N1		N2		N3		N4 ..........
#	    \        /     \         /      \       /  
#		R1		R2		R3         ..........
#	    /        \      /        \      /       \
#	N5		N6		N7		N8 ..........
#	    \        /      \        /      \       / 
#		R4		R5		R6          ..........
#	    /        \      /        \      /       \	
#	N9		N10		N11		N12 ..........
#	.	.	.	.	.	.	.
#	.	.	.	.	.	.	.
#	.	.	.	.	.	.	.
#	.	.	.	.	.	.	.
#	.	.	.	.	.	.	.
#	.	.	.	.	.	.	.
#
##############################


class Grid:
    def __init__(self, num_of_row, num_of_col, ip_node_subnet):
        self.num_of_row = num_of_row
        self.num_of_col = num_of_col
        self.ip_node_subnet = ip_node_subnet
        ###### TOPOLOGY CREATION ######
        self.router=[[]]
        # Creating the routers for the grid topology   
        for i in range(self.num_of_row-1):
            if(i!=0):
                self.router.append([])
            for j in range(self.num_of_col-1):
                self.router[i].append(Router("router" + str(i)+ "_"+ str(j)))
        print("Routers are created")
        # By Default, IP forwarding is enabled for the routers
        # Lists to store all the hosts
        self.hosts=[[]]
        self.ro= self.num_of_row
        self.co= self.num_of_col
        # Creating all the nodes
        for i in range(self.ro):
            if(i!=0):
                self.hosts.append([])
            for j in range(self.co):
                self.hosts[i].append(Node("host" + str(i)+"_"+ str(j)))
        print("Hosts are created")
        # Add connections
        # Lists of tuples to store the interfaces connecting the router and nodes  
        self.connections_for_hosts=[[]]
        #connection for  nodes to router
        for i in range(self.ro):
            if(i!=0):
                self.connections_for_hosts.append([])
            for j in range(self.co):
                temp=[]
                #print(i,j)
                if(i==0 and j!=0 and j!=self.co-1):
                    temp.append(connect(self.hosts[i][j], self.router[0][j-1]))
                    temp.append(connect(self.hosts[i][j], self.router[0][j]))
                    self.connections_for_hosts[i].append(temp)
                elif(i==0 and j==self.co-1):
                    temp.append(connect(self.hosts[i][j], self.router[0][j-1]))
                    self.connections_for_hosts[i].append(temp)
                elif(i==0 and j==0):
                    temp.append(connect(self.hosts[i][j], self.router[0][0]))
                    self.connections_for_hosts[i].append(temp)
                elif(i==self.ro-1 and j!=0 and j!=self.co-1):
                    temp.append(connect(self.hosts[i][j], self.router[self.ro-2][j-1]))
                    temp.append(connect(self.hosts[i][j], self.router[self.ro-2][j]))
                    self.connections_for_hosts[i].append(temp)
                elif(i==self.ro-1 and j==self.co-1):
                    temp.append(connect(self.hosts[i][j], self.router[self.ro-2][j-1]))
                    self.connections_for_hosts[i].append(temp)
                elif(i==self.ro-1 and j==0):
                    temp.append(connect(self.hosts[i][j], self.router[self.ro-2][0]))
                    self.connections_for_hosts[i].append(temp)
                elif(j==0 and i!=0 and i!=self.ro-1):
                    temp.append(connect(self.hosts[i][j], self.router[i-1][0]))
                    temp.append(connect(self.hosts[i][j], self.router[i][0]))
                    self.connections_for_hosts[i].append(temp)
                elif(j==self.co-1 and i!=0 and i!=self.ro-1):
                    temp.append(connect(self.hosts[i][j], self.router[i-1][self.ro-2]))
                    temp.append(connect(self.hosts[i][j], self.router[i][self.ro-2]))
                    self.connections_for_hosts[i].append(temp)
                else:
                    temp.append(connect(self.hosts[i][j], self.router[i-1][j-1]))
                    temp.append(connect(self.hosts[i][j], self.router[i-1][j]))
                    temp.append(connect(self.hosts[i][j], self.router[i][j-1]))
                    temp.append(connect(self.hosts[i][j], self.router[i][j]))
                    self.connections_for_hosts[i].append(temp)
                #print(connections_for_hosts[i][j])
        print("All Interface Connections Made")
        ###### IP ADDRESS ASSIGNMENT ######
        # The IP address is given as input by the user.
        self.node_subnet = Subnet(ip_node_subnet)
        for i in range(self.ro):
            for j in range(self.co):
                if(i==0 and j!=0 and j!=self.co-1):
                    self.node_interface=self.connections_for_hosts[i][j][0][0]
                    self.router_interface=self.connections_for_hosts[i][j][0][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                    self.node_interface=self.connections_for_hosts[i][j][1][0]
                    self.router_interface=self.connections_for_hosts[i][j][1][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                elif(i==0 and j==self.co-1):
                    self.node_interface=self.connections_for_hosts[i][j][0][0]
                    self.router_interface=self.connections_for_hosts[i][j][0][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                elif(i==0 and j==0):
                    self.node_interface=self.connections_for_hosts[i][j][0][0]
                    self.router_interface=self.connections_for_hosts[i][j][0][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                elif(i==self.ro-1 and j!=0 and j!=self.co-1):         
                    self.node_interface=self.connections_for_hosts[i][j][0][0]
                    self.router_interface=self.connections_for_hosts[i][j][0][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                    self.node_interface=self.connections_for_hosts[i][j][1][0]
                    self.router_interface=self.connections_for_hosts[i][j][1][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                elif(i==self.ro-1 and j==self.co-1):            
                    self.node_interface=self.connections_for_hosts[i][j][0][0]
                    self.router_interface=self.connections_for_hosts[i][j][0][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                elif(i==self.ro-1 and j==0):            
                    self.node_interface=self.connections_for_hosts[i][j][0][0]
                    self.router_interface=self.connections_for_hosts[i][j][0][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                elif(j==0 and i!=0 and i!=self.ro-1):           
                    self.node_interface=self.connections_for_hosts[i][j][0][0]
                    self.router_interface=self.connections_for_hosts[i][j][0][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                    self.node_interface=self.connections_for_hosts[i][j][1][0]
                    self.router_interface=self.connections_for_hosts[i][j][1][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                elif(j==self.co-1 and i!=0 and i!=self.ro-1):            
                    self.node_interface=self.connections_for_hosts[i][j][0][0]
                    self.router_interface=self.connections_for_hosts[i][j][0][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                    self.node_interface=self.connections_for_hosts[i][j][1][0]
                    self.router_interface=self.connections_for_hosts[i][j][1][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                else:            
                    self.node_interface=self.connections_for_hosts[i][j][0][0]
                    self.router_interface=self.connections_for_hosts[i][j][0][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                    self.node_interface=self.connections_for_hosts[i][j][1][0]
                    self.router_interface=self.connections_for_hosts[i][j][1][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                    self.node_interface=self.connections_for_hosts[i][j][2][0]
                    self.router_interface=self.connections_for_hosts[i][j][2][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
                    self.node_interface=self.connections_for_hosts[i][j][3][0]
                    self.router_interface=self.connections_for_hosts[i][j][3][1]
                    print(self.node_subnet.get_next_addr())
                    # Assigning addresses to the interfaces
                    self.node_interface.set_address(self.node_subnet.get_next_addr())
                    self.router_interface.set_address(self.node_subnet.get_next_addr())
        print("Addresses are assigned to all interfaces including router interfaces.")
            ####### ROUTING #######
            # If any packet needs to be sent from any nodes, send it to the connected routers
            # via their own interface.  Node1 -->> interface_of_node1 -->> self.router_interface
        
        for j in range(self.co):
            if(i==0 and j!=0 and j!=self.co-1):            
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][0][0])
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][1][0])
                self.router[0][j-1].add_route(self.connections_for_hosts[i][j][0][0].get_address(), self.connections_for_hosts[i][j][0][1])
                self.router[0][j].add_route(self.connections_for_hosts[i][j][1][0].get_address(), self.connections_for_hosts[i][j][1][1])
            elif(i==0 and j==self.co-1):        
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][0][0])
                self.router[0][j-1].add_route(self.connections_for_hosts[i][j][0][0].get_address(), self.connections_for_hosts[i][j][0][1])
                
            elif(i==0 and j==0):            
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][0][0])
                self.router[0][0].add_route(self.connections_for_hosts[i][j][0][0].get_address(), self.connections_for_hosts[i][j][0][1])
            
            elif(i==self.ro-1 and j!=0 and j!=self.co-1):            
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][0][0])
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][1][0])
                self.router[self.ro-2][j-1].add_route(self.connections_for_hosts[i][j][0][0].get_address(), self.connections_for_hosts[i][j][0][1])
                self.router[self.ro-2][j].add_route(self.connections_for_hosts[i][j][1][0].get_address(), self.connections_for_hosts[i][j][1][1])
                
            elif(i==self.ro-1 and j==self.co-1):            
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][0][0])
                self.router[self.ro-2][j-1].add_route(self.connections_for_hosts[i][j][0][0].get_address(), self.connections_for_hosts[i][j][0][1])
                
            elif(i==self.ro-1 and j==0):            
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][0][0])
                self.router[self.ro-2][0].add_route(self.connections_for_hosts[i][j][0][0].get_address(), self.connections_for_hosts[i][j][0][1])
            
            elif(j==0 and i!=0 and i!=self.ro-1):            
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][0][0])
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][1][0])
                self.router[i-1][0].add_route(self.connections_for_hosts[i][j][0][0].get_address(), self.connections_for_hosts[i][j][0][1])
                self.router[i][0].add_route(self.connections_for_hosts[i][j][1][0].get_address(), self.connections_for_hosts[i][j][1][1])
                
            elif(j==self.co-1 and i!=0 and i!=self.ro-1):            
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][0][0])
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][1][0])
                self.router[i-1][self.ro-2].add_route(self.connections_for_hosts[i][j][0][0].get_address(), self.connections_for_hosts[i][j][0][1])
                self.router[i][self.ro-2].add_route(self.connections_for_hosts[i][j][1][0].get_address(), self.connections_for_hosts[i][j][1][1])
                
            else:            
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][0][0])
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][1][0])
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][2][0])
                self.hosts[i][j].add_route("DEFAULT", self.connections_for_hosts[i][j][3][0])
                self.router[i-1][j-1].add_route(self.connections_for_hosts[i][j][0][0].get_address(), self.connections_for_hosts[i][j][0][1])
                self.router[i-1][j].add_route(self.connections_for_hosts[i][j][1][0].get_address(), self.connections_for_hosts[i][j][1][1])
                self.router[i][j-1].add_route(self.connections_for_hosts[i][j][2][0].get_address(), self.connections_for_hosts[i][j][2][1])
                self.router[i][j].add_route(self.connections_for_hosts[i][j][3][0].get_address(), self.connections_for_hosts[i][j][3][1])
                
            #print("Done")
    def node_link_setup(self, bandwidth:str, delay:str):
        ############LINK SET UP##################
        # Setting up the attributes of the connections between
		
        for i in range(self.ro):
            for j in range(self.co):
                if(i==0 and j!=0 and j!=self.co-1):            
                        self.connections_for_hosts[i][j][0][0].set_attributes(bandwidth, delay)
                        self.connections_for_hosts[i][j][0][1].set_attributes(bandwidth, delay)
                        self.connections_for_hosts[i][j][1][0].set_attributes(bandwidth, delay)
                        self.connections_for_hosts[i][j][1][1].set_attributes(bandwidth, delay)
                elif(i==0 and j==self.co-1):        
                    self.connections_for_hosts[i][j][0][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][0][1].set_attributes(bandwidth, delay)
                elif(i==0 and j==0):           
                    self.connections_for_hosts[i][j][0][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][0][1].set_attributes(bandwidth, delay)
                elif(i==self.ro-1 and j!=0 and j!=self.co-1):            
                    self.connections_for_hosts[i][j][0][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][0][1].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][1][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][1][1].set_attributes(bandwidth, delay)
                elif(i==self.ro-1 and j==self.co-1):            
                    self.connections_for_hosts[i][j][0][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][0][1].set_attributes(bandwidth, delay)
                elif(i==self.ro-1 and j==0):            
                    self.connections_for_hosts[i][j][0][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][0][1].set_attributes(bandwidth, delay)
                elif(j==0 and i!=0 and i!=self.ro-1):            
                    self.connections_for_hosts[i][j][0][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][0][1].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][1][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][1][1].set_attributes(bandwidth, delay)
                elif(j==self.co-1 and i!=0 and i!=self.ro-1):            
                    self.connections_for_hosts[i][j][0][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][0][1].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][1][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][1][1].set_attributes(bandwidth, delay)
                else:            
                    self.connections_for_hosts[i][j][0][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][0][1].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][1][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][1][1].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][2][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][2][1].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][3][0].set_attributes(bandwidth, delay)
                    self.connections_for_hosts[i][j][3][1].set_attributes(bandwidth, delay)

